$Id

SIGNAL MODULE
-------------

The Signal module intends to provide a UI wherein the user will be able to
create signals (sic) for stuff like node IDs, node types, paths, user IDs,
roles, taxonomies and so on. Signals will allow for ranges, logical operators,
regexes etc. Modules such as node_style (which would have then become a
misnomer) would register with these signals and would be invoked whenever a
signal match occurs.

The idea for Signal originated with Berkes and Zen. See
http://drupal.org/project/comments/add/160611

At the current time the module is in the planning stages. Interest, feature
requests, UI suggestions, etc. are welcome - submit an issue!